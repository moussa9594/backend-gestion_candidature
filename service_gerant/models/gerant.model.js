const mongoose = require("mongoose");

module.exports = mongoose => {
    const Gerant = mongoose.model(
      "gerant",
      mongoose.Schema(
        {
          id: String,
          prenom: String,
          nom: String,
          email: String,
          adresse: String,
          telephone: String,
          genre: String,
          profession: String,
          username: String,
          password: String,
        },
        { strict: false } 
      )
    );
  
    return Gerant;
  };


 

  