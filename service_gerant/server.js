const express = require("express")
const bodyParser = require("body-parser")
const cors = require("cors")
const router = require("./routes/gerant.route")
const eurekaHelper = require('./config/eureka-helper')


const app = express()

var corsOptions = {
  origin: "*"
}

app.use(cors(corsOptions))
//app.use(function (req, res, next) {
//  res.header('Access-Control-Allow-Origin', '*');
 // res.header('Access-Control-Allow-Headers', '*');
  //next();
//});

// parse requests of content-type - application/json
app.use(bodyParser.json({ limit: '150mb', extended: true }))
// bodyParser = {
//   json: {limit: '50mb', extended: true},
//   urlencoded: {limit: '50mb', extended: true}
// };

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))

app.use(router)



// set port, listen for requests
const PORT = process.env.PORT || 8082
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`)
})

const db = require("./models")
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Connected to the database!")
  })
  .catch(err => {
    console.log("Cannot connect to the database!", err)
    process.exit()
  })

//eurekaHelper.registerWithEureka('gerant-service', PORT);
