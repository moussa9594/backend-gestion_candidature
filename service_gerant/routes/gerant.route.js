const gerant = require("../controllers/gerant.controller.js");


var router = require("express").Router();

// Create a new Tutorial
router.get("/", (req, res) => {
    res.send('we are on gerant service 1');
});

router.post("/gerant", gerant.create);

// Retrieve all gerant
router.get("/gerant", gerant.findAll);



// Retrieve a single Tutorial with id
router.get("/gerant/:gerantID", gerant.findOne);

// Retrieve a single Tutorial with id
router.get("/gerant/username/:username", gerant.findOneByLogin);




module.exports = router;

