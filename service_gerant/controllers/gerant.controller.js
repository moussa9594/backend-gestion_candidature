const db = require("../models")

const Gerant = db.gerant


  
  // Create and Save a new gerant
 exports.create = (req, res) => {
  
  // Create a gerant
   const gerant = new Gerant(req.body)

  // Save gerant in the database
  gerant
    .save(gerant)
    .then(data => {
      res.send(data)
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the gerant."
      })
    })
}


// Retrieve all gerants from the database.
exports.findAll = (req, res) => {
  Gerant.find()
  .then(data => {
    res.send(data)
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while fetching the gerant."
    })
  })
}

// Find a single gerant with an id
exports.findOne = (req, res) => {
  Gerant.findOne({_id: req.params.gerantID})
  .then(data => {
    res.send(data)
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while fetching the gerant."
    })
  })
}

// Find a single gerant with an id
exports.findOneByLogin = (req, res) => {
  Gerant.findOne({username: req.params.username})
  .then(data => {
    res.send(data)
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while fetching the gerant."
    })
  })
}






