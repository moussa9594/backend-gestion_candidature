const db = require("../models")

const Concours = db.concours

// Retrieve all concours from the database.
exports.findAll = (req, res) => {
  Concours.find()
  .then(data => {
    res.send(data)
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while fetching the concours."
    })
  })
}

  // Create and Save a new concours
 exports.create = (req, res) => {
   const concours = new Concours(req.body)

  concours
    .save(concours)
    .then(data => {
      res.send(data)
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the concours."
      })
    })
}




// Find a single concours with an id
exports.findOne = (req, res) => {
  Concours.findOne({"gerant._id": req.params.gerantID})
  .then(data => {
    res.send(data)
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while fetching the concours."
    })
  })
}

// put a  concours 
exports.putConcours = (req, res) => {
  const newConcours = new Concours(req.body)
  Concours.findByIdAndUpdate({_id: req.body._id}, newConcours)
  .then(data => {
    res.send(data)
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while fetching the concours."
    })
  })
}

exports.findConcours = (req, res) => {
  Concours.findOne({_id: req.params.id})
  .then(data => {
    res.send(data)
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while fetching the concours."
    })
  })
}

exports.findFile = (req, res) => {
  Concours.findOne({_id: req.params.id})
  .then(data => {
    res.contentType('application/pdf')
    res.send(data.data)
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while fetching the candidat."
    })
  })
}

// Delete a concours with the specified id in the request
exports.delete = (req, res) => {
  
}

// Delete all concourss from the database.
exports.deleteAll = (req, res) => {
  
}


