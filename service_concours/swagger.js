module.exports = {
  openapi: "3.0.3", // present supported openapi version
  info: {
    title: "Simple Todos API", // short title.
    description: "A simple todos API", //  desc.
    version: "1.0.0", // version number
    contact: {
      name: "John doe", // your name
      email: "john@web.com", // your email
      url: "web.com", // your website
    },
  },
  get: {
    tags: ["Todo CRUD operations"], // operation's tag.
    description: "Get a todo", // operation's desc.
    operationId: "getTodo", // unique operation id
    parameters: [
      // expected params.
      {
        name: "id", // name of the param
        in: "path", // location of the param
        // schema: {
        //   $ref: "#/components/schemas/id", // data model of the param
        // },
        required: true, // Mandatory param
        description: "A single todo id", // param desc.
      },
    ],
    // expected responses
    responses: {
      // response code
      200: {
        description: "Todo is obtained", // response desc.
        content: {
          // content-type
          "application/json": {
            schema: {
              $ref: "#/components/schemas/Todo", // todo data model
            },
          },
        },
      },
      // response code
      404: {
        description: "Todo is not found", // response desc.
        content: {
          // content-type
          "application/json": {
            schema: {
              $ref: "#/components/schemas/Error", // error data model
            },
          },
        },
      },
    },
  },
}

