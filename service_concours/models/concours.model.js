const mongoose = require("mongoose");

module.exports = mongoose => {
    const Concours = mongoose.model(
      "concours",
      mongoose.Schema(
        {
          libelle: String, 
          periode: Number,
          filePath: String,
          data: mongoose.Mixed,
          date_cloture: Date,
          cloturer: Boolean,
          gerant: Gerant,
          niveaux: [Niveau],
          lancer: Boolean
        },
        { strict: false } 
      )
    );
  
    return Concours;
  };


  const Gerant = new mongoose.Schema({
    // some schema definition here
          id: String,
          prenom: String,
          nom: String,
          email: String,
          adresse: String,
          telephone: String,
          genre: String,
          profession: String,
          username: String,
          password: String,
  },{ strict: false });

  const Piece = new mongoose.Schema({
    // some schema definition here
          id: String,
          libelle: String,
          required: Boolean,
          type: String
  },{ strict: false });

  const Niveau = new mongoose.Schema({
    // some schema definition here
          id: String,
          nom_niveau: String,
          pieces: [Piece]
  },{ strict: false });

  