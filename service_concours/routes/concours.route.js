const concours = require("../controllers/concours.controller.js");


var router = require("express").Router();

// Create a new Tutorial
router.get("/", (req, res) => {
    res.send('Nous sommes dans le service concours 1');
});

router.post("/concours", concours.create);

// Retrieve all concours
router.get("/concours", concours.findAll);



// Retrieve a concours's file
router.get("/concours/:id", concours.findFile);

// Modified a concours
router.put("/concours", concours.putConcours);


// Retrieve a concours's file
router.get("/concours/id/:id", concours.findConcours);

// Retrieve a single Tutorial with id
router.get("/concours/gerant/:gerantID", concours.findOne);



// Delete a Tutorial with id
router.delete("/concours/delete/:id", concours.delete);



module.exports = router;

