const db = require("../models")
const fs = require('fs');
const path = require('path');
const base64 = require('base64topdf');
const pdf2base64 = require('pdf-to-base64');
const Candidature = db.candidature;
const base_folder = 'assets/data'
var nodemailer = require('nodemailer');
const { json } = require("body-parser");
var transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: 'mmoussaa94@gmail.com',
    pass: 'mami2000moussa'
  }
});


// Create and Save a new Candidat
exports.create = (req, res) => {


  // Create a candidat
  const candidature = new Candidature(req.body)

  // Save candidat in the database
  candidature
    .save(candidature)
    .then(data => {
      if (data) {
        // create id_concours folder
        fs.mkdir(base_folder + '/concours/' + req.body.idConcours, { recursive: true }, (err) => {
          if (err) throw err;
          // create periode_n folder
          fs.mkdir(base_folder + '/concours/' + req.body.idConcours + '/periodes/' + req.body.periode, { recursive: true }, (err) => {
            if (err) throw err;
            // create niveau folder
            fs.mkdir(base_folder + '/concours/' + req.body.idConcours + '/periodes/' + req.body.periode + '/niveaux/' + req.body.niveau_postuler, { recursive: true }, (err) => {
              if (err) throw err;
              // create candidat folder
              fs.mkdir(base_folder + '/concours/' + req.body.idConcours + '/periodes/' + req.body.periode + '/niveaux/' + req.body.niveau_postuler + '/candidats/' + req.body.numeroCandidature, { recursive: true }, (err) => {
                if (err) throw err;
                const path = base_folder + '/concours/' + req.body.idConcours + '/periodes/' + req.body.periode + '/niveaux/' + req.body.niveau_postuler + '/candidats/' + req.body.numeroCandidature
                if (err) return console.error(err)
                console.log(req.body.piecesFournis[0])
                req.body.piecesFournis.forEach(piece => {
                  let n = 0
                  let buff = new Buffer.from(piece.fichier.split(',')[1], 'base64');
                  fs.writeFileSync(path + '/' + n + '.pdf', buff);
                  n++
                });


              });
            });
          });
        });
        res.send(data)
      }
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the candidat."
      })
    })
}


// Retrieve all Candidats from the database.
exports.send = (req, res) => {

  if (!req.body.payer) {
    var mailOptions = {
      from: 'mmoussaa94@gmail.com',
      to: req.body.mail_candidat,
      subject: 'Candidature au ' + req.body.nom_concours,
      text: 'Bonjour ' + req.body.prenom + ' ' + req.body.nom + ',\nVotre candidature a été bien enregistrée. Numéro de candidature: ' + req.body.numero + '. \nVeuillez finaliser votre candidature en payant les frais de dossier.\nNB: pour rechercher votre dossier, renseignez le numéro de candidature dans la barre de recherche une fois que vous ayez cliqué sur le concours concerné.'
    };

  } else {
    var mailOptions = {
      from: 'mmoussaa94@gmail.com',
      to: req.body.mail_candidat,
      subject: 'Candidature au ' + req.body.nom_concours,
      text: 'Bonjour ' + req.body.prenom + ' ' + req.body.nom + ',\nVotre candidature a été finalisée avec succès. Nous vous suggérons de consulter régulièrement votre dossier pour suivre son état.',
      attachments: [
        {
          filename: 'Frais_dossiers.pdf',
          path: req.body.url
        }
      ]
    };


  }



  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
      res.send(info.response)
    }
  });

}

// Find a single Candidat with an id findByIdAndPeriode
exports.findOne = (req, res) => {
  Candidature.findOne({ idConcours: req.params.idConcours })
    .then(data => {
      res.send(data)
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while fetching the candidat."
      })
    })
}


// Update a Candidat by the id in the request
exports.update = (req, res) => {
  console.log('in update');
  const newCandidature = new Candidature(req.body)
  Candidature.findByIdAndUpdate({ _id: req.body._id }, newCandidature)
    .then(data => {
      if (data) {
        // create id_concours folder
        fs.mkdir(base_folder + '/concours/' + req.body.idConcours, { recursive: true }, (err) => {
          if (err) throw err;
          // create periode_n folder
          fs.mkdir(base_folder + '/concours/' + req.body.idConcours + '/periodes/' + req.body.periode, { recursive: true }, (err) => {
            if (err) throw err;
            // create niveau folder
            fs.mkdir(base_folder + '/concours/' + req.body.idConcours + '/periodes/' + req.body.periode + '/niveaux/' + req.body.niveau_postuler, { recursive: true }, (err) => {
              if (err) throw err;
              // create candidat folderz
              fs.mkdir(base_folder + '/concours/' + req.body.idConcours + '/periodes/' + req.body.periode + '/niveaux/' + req.body.niveau_postuler + '/candidats/' + req.body.numeroCandidature, { recursive: true }, (err) => {
                if (err) throw err;
                const path = base_folder + '/concours/' + req.body.idConcours + '/periodes/' + req.body.periode + '/niveaux/' + req.body.niveau_postuler + '/candidats/' + req.body.numeroCandidature
                if (err) return console.error(err)
                let n = 0
                try {

                  req.body.piecesFournis.forEach(piece => {
                    let buff = new Buffer.from(piece.fichier.split(',')[1], 'base64');
                    fs.writeFileSync(path + '/' + n + '.pdf', buff);
                    n++
                  });
                } catch (e) {

                }


              });
            });
          });
        });
        res.send(data)
      }
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while fetching the concours."
      })
    })
}

exports.getFiles = (req, res) => {

  pdf2base64(base_folder + '/concours/' + req.body.idConcours + '/periodes/' +
    req.body.periode + '/niveaux/' + req.body.niveau + '/candidats/' + req.body.numeroCandidature + '/Une photocopie légalisée de l\'attestation de Baccalauréat de l\'année en cours.pdf')
    .then(
      (response) => {
        console.log(response); //cGF0aC90by9maWxlLmpwZw==
        res.send(JSON.stringify(response))
        window.open('_blank')
      }
    )
    .catch(
      (error) => {
        console.log(error); //Exepection error....
      }
    )
}

// Delete a Candidat with the specified id in the request


