const mongoose = require("mongoose");


module.exports = mongoose => {
  const Candidature = mongoose.model(
    "candidature",
    mongoose.Schema(
      {
        idConcours: String,
        periodes: [Periode]
      }
    )
  );

  return Candidature;
};

const PieceFourni = new mongoose.Schema(
  {
    libelle: String,
    fichier: mongoose.Mixed,
    valid: Number
  },
  { strict: false }
);

const Candidat = new mongoose.Schema(
  {
    numeroCandidature: {
      type: String,
      unique: true // `numeroCandidature` must be unique
    },
    prenom: String,
    nom: String,
    telephone: String,
    email: String,
    etablissement: String,
    adresse: String,
    preselectionner: Boolean,
    a_corriger: Boolean,
    note: Number,
    motif: String,
    piecesFournis: [PieceFourni]
  }, { strict: false });


const NiveauPostuler = new mongoose.Schema(
  {
    nom_niveau: String,
    candidats: [Candidat]
  }, { strict: false });

const Periode = new mongoose.Schema(
  {
    annee: Number,
    niveaux: [NiveauPostuler]
  }, { strict: false });



