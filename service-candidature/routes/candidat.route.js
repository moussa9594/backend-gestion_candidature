const candidature = require("../controllers/candidat.controller.js");
const multipart = require('connect-multiparty')

var router = require("express").Router();

// Create a new Candidature
router.get("/", (req, res) => {
    res.send('we are on candidature service 1');
});

router.post("/candidature", candidature.create);

// Retrieve all candidat
router.post("/candidature/send", candidature.send);

// Retrieve a single Candidature with id
router.get("/candidature/:idConcours", candidature.findOne);

router.post("/candidature/getFiles", candidature.getFiles);

// Update a Candidature with id
router.put("/candidature", candidature.update);


// Delete a Candidature with id
// router.get("/candidature/mail", candidature.send);



module.exports = router;

